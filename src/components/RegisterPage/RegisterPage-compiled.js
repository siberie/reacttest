'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _RegisterPage = require('./RegisterPage.css');

var _RegisterPage2 = _interopRequireDefault(_RegisterPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var title = 'New User Registration'; /**
                                      * React Starter Kit (https://www.reactstarterkit.com/)
                                      *
                                      * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                      *
                                      * This source code is licensed under the MIT license found in the
                                      * LICENSE.txt file in the root directory of this source tree.
                                      */

var RegisterPage = function (_Component) {
  (0, _inherits3.default)(RegisterPage, _Component);

  function RegisterPage() {
    (0, _classCallCheck3.default)(this, RegisterPage);
    return (0, _possibleConstructorReturn3.default)(this, (RegisterPage.__proto__ || (0, _getPrototypeOf2.default)(RegisterPage)).apply(this, arguments));
  }

  (0, _createClass3.default)(RegisterPage, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.context.onSetTitle(title);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: _RegisterPage2.default.root },
        _react2.default.createElement(
          'div',
          { className: _RegisterPage2.default.container },
          _react2.default.createElement(
            'h1',
            null,
            title
          ),
          _react2.default.createElement(
            'p',
            null,
            '...'
          )
        )
      );
    }
  }]);
  return RegisterPage;
}(_react.Component);

RegisterPage.contextTypes = {
  onSetTitle: _react.PropTypes.func.isRequired
};
exports.default = (0, _withStyles2.default)(RegisterPage, _RegisterPage2.default);

//# sourceMappingURL=RegisterPage-compiled.js.map