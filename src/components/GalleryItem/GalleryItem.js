import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames'
import s from './GalleryItem.css';
import BoxShadowed from '../BoxShadowed'

class GalleryItem extends Component {
  static propTypes = {
    item: PropTypes.object.isRequired
  };


  render() {
    return (
      <BoxShadowed {...this.props} className={s.root}>
        <div className={s.wrapper}>
          <image className={s.content} src={this.props.item.image}/>
        </div>
      </BoxShadowed>
    );
  }

}

export default withStyles(GalleryItem, s);
