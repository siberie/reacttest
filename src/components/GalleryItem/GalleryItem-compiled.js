'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _GalleryItem = require('./GalleryItem.css');

var _GalleryItem2 = _interopRequireDefault(_GalleryItem);

var _BoxShadowed = require('../BoxShadowed');

var _BoxShadowed2 = _interopRequireDefault(_BoxShadowed);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GalleryItem = function (_Component) {
  (0, _inherits3.default)(GalleryItem, _Component);

  function GalleryItem() {
    (0, _classCallCheck3.default)(this, GalleryItem);
    return (0, _possibleConstructorReturn3.default)(this, (GalleryItem.__proto__ || (0, _getPrototypeOf2.default)(GalleryItem)).apply(this, arguments));
  }

  (0, _createClass3.default)(GalleryItem, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _BoxShadowed2.default,
        (0, _extends3.default)({}, this.props, { className: _GalleryItem2.default.root }),
        _react2.default.createElement(
          'div',
          { className: _GalleryItem2.default.wrapper },
          _react2.default.createElement('image', { className: _GalleryItem2.default.content, src: this.props.item.image })
        )
      );
    }
  }]);
  return GalleryItem;
}(_react.Component);

GalleryItem.propTypes = {
  item: _react.PropTypes.object.isRequired
};
exports.default = (0, _withStyles2.default)(GalleryItem, _GalleryItem2.default);

//# sourceMappingURL=GalleryItem-compiled.js.map