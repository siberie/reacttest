'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _SideMenu = require('./SideMenu.css');

var _SideMenu2 = _interopRequireDefault(_SideMenu);

var _Location = require('../../core/Location');

var _Location2 = _interopRequireDefault(_Location);

var _Link = require('../Link');

var _Link2 = _interopRequireDefault(_Link);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

var SideMenu = function (_Component) {
  (0, _inherits3.default)(SideMenu, _Component);

  function SideMenu(props) {
    (0, _classCallCheck3.default)(this, SideMenu);

    // Location.
    var _this = (0, _possibleConstructorReturn3.default)(this, (SideMenu.__proto__ || (0, _getPrototypeOf2.default)(SideMenu)).call(this, props));

    _this.state = { active: 0, expanded: _this.props.expanded };
    _this.hide = _this.hide.bind(_this);
    _this.expand = _this.expand.bind(_this);
    _this.switchExpanded = _this.switchExpanded.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(SideMenu, [{
    key: 'onPressed',
    value: function onPressed(index) {
      console.log(index);

      this.setState({ active: index });
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.setState({ expanded: false });
    }
  }, {
    key: 'expand',
    value: function expand() {
      this.setState({ expanded: true });
    }
  }, {
    key: 'switchExpanded',
    value: function switchExpanded() {
      if (this.state.expanded && this.props.expanded) {
        this.hide();
      } else {
        this.expand();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var button;
      if (this.props.expanded) {
        button = _react2.default.createElement('div', { className: _SideMenu2.default.expand_button, onClick: this.switchExpanded });
      }
      return _react2.default.createElement(
        'div',
        { className: this.state.expanded && this.props.expanded ? (0, _classnames2.default)(_SideMenu2.default.menu, _SideMenu2.default.expanded) : (0, _classnames2.default)(_SideMenu2.default.menu, _SideMenu2.default.hidden) },
        this.props.children.map(function (child, idx) {
          var isActive = idx == _this2.state.active;
          console.log(isActive);
          var element = _react2.default.cloneElement(child, {
            selected: isActive,
            onClick: _this2.onPressed.bind(_this2, idx),
            key: child.props.key || idx,
            expanded: _this2.state.expanded && _this2.props.expanded
          });
          return element;
        }),
        button
      );
    }
  }]);
  return SideMenu;
}(_react.Component);

SideMenu.propTypes = {
  alignment: _react.PropTypes.string.isRequired,
  expanded: _react.PropTypes.bool
};
exports.default = (0, _withStyles2.default)(SideMenu, _SideMenu2.default);

//# sourceMappingURL=SideMenu-compiled.js.map