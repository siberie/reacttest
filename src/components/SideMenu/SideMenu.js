/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SideMenu.css';
import Location from '../../core/Location';
import Link from '../Link';
import cx from 'classnames'

class SideMenu extends Component {

  static propTypes = {
    alignment: PropTypes.string.isRequired,
    expanded: PropTypes.bool
  };

  constructor(props) {
    super(props);
    // Location.
    this.state = {active: 0, expanded: this.props.expanded};
    this.hide = this.hide.bind(this);
    this.expand = this.expand.bind(this);
    this.switchExpanded = this.switchExpanded.bind(this);
  }

  onPressed(index) {
    console.log(index);

    this.setState({active: index});
  }

  hide() {
    this.setState({expanded: false});
  }

  expand() {
    this.setState({expanded: true});
  }

  switchExpanded() {
    if (this.state.expanded && this.props.expanded) {
      this.hide();
    } else {
      this.expand();
    }
  }

  render() {
    var button;
    if(this.props.expanded) {
      button = <div className={s.expand_button} onClick={this.switchExpanded}></div>
    }
    return (
      <div className={this.state.expanded && this.props.expanded ? cx(s.menu, s.expanded) : cx(s.menu, s.hidden)}>
        {this.props.children.map((child, idx) => {
          var isActive = idx == this.state.active;
          console.log(isActive);
          var element = React.cloneElement(child, {
            selected: isActive,
            onClick: this.onPressed.bind(this, idx),
            key: child.props.key || idx,
            expanded: this.state.expanded && this.props.expanded
          });
          return element;
        })}
        {button}
      </div>
    )
  }
}


export default withStyles(SideMenu, s);
