'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ContentPage = require('./ContentPage.css');

var _ContentPage2 = _interopRequireDefault(_ContentPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ContentPage = function (_Component) {
  (0, _inherits3.default)(ContentPage, _Component);

  function ContentPage() {
    (0, _classCallCheck3.default)(this, ContentPage);
    return (0, _possibleConstructorReturn3.default)(this, (ContentPage.__proto__ || (0, _getPrototypeOf2.default)(ContentPage)).apply(this, arguments));
  }

  (0, _createClass3.default)(ContentPage, [{
    key: 'render',
    value: function render() {
      this.context.onSetTitle(this.props.title);
      return _react2.default.createElement(
        'div',
        { className: _ContentPage2.default.root },
        _react2.default.createElement(
          'div',
          { className: _ContentPage2.default.container },
          this.props.path === '/' ? null : _react2.default.createElement(
            'h1',
            null,
            this.props.title
          ),
          _react2.default.createElement('div', { dangerouslySetInnerHTML: { __html: this.props.content || '' } })
        )
      );
    }
  }]);
  return ContentPage;
}(_react.Component); /**
                      * React Starter Kit (https://www.reactstarterkit.com/)
                      *
                      * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                      *
                      * This source code is licensed under the MIT license found in the
                      * LICENSE.txt file in the root directory of this source tree.
                      */

ContentPage.propTypes = {
  path: _react.PropTypes.string.isRequired,
  content: _react.PropTypes.string.isRequired,
  title: _react.PropTypes.string
};
ContentPage.contextTypes = {
  onSetTitle: _react.PropTypes.func.isRequired
};
exports.default = (0, _withStyles2.default)(ContentPage, _ContentPage2.default);

//# sourceMappingURL=ContentPage-compiled.js.map