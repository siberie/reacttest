/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Designer.css';
import SquareTile from '../BoxShadowed';

const title = 'Targets';

class Designer extends Component {

  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.context.onSetTitle(title);
  }

  render() {
    return (
      <div className={s.root}>
        {this.props.data.map((item) => {
          return (<SquareTile item={item}/>);
        })}
      </div>
    );
  }

}

export default withStyles(Designer, s);
