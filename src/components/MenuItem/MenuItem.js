import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames'
import s from './MenuItem.css';
import Link from '../Link';

class MenuItem extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    expanded: PropTypes.bool,
    image: PropTypes.object
  };

  render() {
    const {selected, ...props} = this.props;
    var key = this.props.key || this.props.label;
    var label = '';

    if(this.props.expanded === true) {
      label = <p className={s.label}>{this.props.label}</p>
    }

    var image;
    if(this.props.image) {
      const {file, x, y, width, height, ...image_props} = this.props.image;
      var style = {
        backgroundPosition: [x + 'px',y + 'px'].join(' '),
        width: width,
        height: height,
        backgroundImage: `url("${file}")`
    };

      image = <div {...image_props} className={s.icon} style={style}/>;
    }
    var classes = new Array(s.root);
    if(this.props.expanded === true) classes.push(s.expanded); else classes.push(s.hidden);
    if(this.props.selected === true) classes.push(s.active);
    console.log(classes);
    return (
      <Link {...props} key={key} className={cx(classes)}>
        {label}
        <div className={s.overlay}/>
        {image}
        {this.props.children}
      </Link>
    );
  }
}

class ExpandMenuItem extends Component {

}

export default withStyles(MenuItem, s);
