'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _MenuItem = require('./MenuItem.css');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _Link = require('../Link');

var _Link2 = _interopRequireDefault(_Link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MenuItem = function (_Component) {
  (0, _inherits3.default)(MenuItem, _Component);

  function MenuItem() {
    (0, _classCallCheck3.default)(this, MenuItem);
    return (0, _possibleConstructorReturn3.default)(this, (MenuItem.__proto__ || (0, _getPrototypeOf2.default)(MenuItem)).apply(this, arguments));
  }

  (0, _createClass3.default)(MenuItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props;
      var selected = _props.selected;
      var props = (0, _objectWithoutProperties3.default)(_props, ['selected']);

      var key = this.props.key || this.props.label;
      var label = '';

      if (this.props.expanded === true) {
        label = _react2.default.createElement(
          'p',
          { className: _MenuItem2.default.label },
          this.props.label
        );
      }

      var image;
      if (this.props.image) {
        var _props$image = this.props.image;
        var file = _props$image.file;
        var x = _props$image.x;
        var y = _props$image.y;
        var width = _props$image.width;
        var height = _props$image.height;
        var image_props = (0, _objectWithoutProperties3.default)(_props$image, ['file', 'x', 'y', 'width', 'height']);

        var style = {
          backgroundPosition: [x + 'px', y + 'px'].join(' '),
          width: width,
          height: height,
          backgroundImage: 'url("' + file + '")'
        };

        image = _react2.default.createElement('div', (0, _extends3.default)({}, image_props, { className: _MenuItem2.default.icon, style: style }));
      }
      var classes = new Array(_MenuItem2.default.root);
      if (this.props.expanded === true) classes.push(_MenuItem2.default.expanded);else classes.push(_MenuItem2.default.hidden);
      if (this.props.selected === true) classes.push(_MenuItem2.default.active);
      console.log(classes);
      return _react2.default.createElement(
        _Link2.default,
        (0, _extends3.default)({}, props, { key: key, className: (0, _classnames2.default)(classes) }),
        label,
        _react2.default.createElement('div', { className: _MenuItem2.default.overlay }),
        image,
        this.props.children
      );
    }
  }]);
  return MenuItem;
}(_react.Component);

MenuItem.propTypes = {
  label: _react.PropTypes.string.isRequired,
  href: _react.PropTypes.oneOfType([_react.PropTypes.string, _react.PropTypes.object]),
  expanded: _react.PropTypes.bool,
  image: _react.PropTypes.object
};

var ExpandMenuItem = function (_Component2) {
  (0, _inherits3.default)(ExpandMenuItem, _Component2);

  function ExpandMenuItem() {
    (0, _classCallCheck3.default)(this, ExpandMenuItem);
    return (0, _possibleConstructorReturn3.default)(this, (ExpandMenuItem.__proto__ || (0, _getPrototypeOf2.default)(ExpandMenuItem)).apply(this, arguments));
  }

  return ExpandMenuItem;
}(_react.Component);

exports.default = (0, _withStyles2.default)(MenuItem, _MenuItem2.default);

//# sourceMappingURL=MenuItem-compiled.js.map