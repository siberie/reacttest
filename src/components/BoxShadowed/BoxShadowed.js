import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames'
import s from './BoxShadowed.css';

class BoxShadowed extends Component {

  render() {
    return (
      <div {...this.props} className={cx(this.props.className, s.root)}>
        {this.props.children}
      </div>
    );
  }

}

export default withStyles(BoxShadowed, s);
