'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Header = require('./Header.css');

var _Header2 = _interopRequireDefault(_Header);

var _Link = require('../Link');

var _Link2 = _interopRequireDefault(_Link);

var _Navigation = require('../Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Header = function (_Component) {
  (0, _inherits3.default)(Header, _Component);

  function Header() {
    (0, _classCallCheck3.default)(this, Header);
    return (0, _possibleConstructorReturn3.default)(this, (Header.__proto__ || (0, _getPrototypeOf2.default)(Header)).apply(this, arguments));
  }

  (0, _createClass3.default)(Header, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: _Header2.default.root },
        _react2.default.createElement(
          'div',
          { className: _Header2.default.container },
          _react2.default.createElement(_Navigation2.default, { className: _Header2.default.nav }),
          _react2.default.createElement(
            _Link2.default,
            { className: _Header2.default.brand, to: '/' },
            _react2.default.createElement('img', { src: require('./logo-small.png'), width: '38', height: '38', alt: 'React' }),
            _react2.default.createElement(
              'span',
              { className: _Header2.default.brandTxt },
              'LucidLab'
            )
          )
        )
      );
    }
  }]);
  return Header;
}(_react.Component); /**
                      * React Starter Kit (https://www.reactstarterkit.com/)
                      *
                      * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                      *
                      * This source code is licensed under the MIT license found in the
                      * LICENSE.txt file in the root directory of this source tree.
                      */

exports.default = (0, _withStyles2.default)(Header, _Header2.default);

//# sourceMappingURL=Header-compiled.js.map