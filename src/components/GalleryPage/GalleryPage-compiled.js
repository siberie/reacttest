'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _GalleryPage = require('./GalleryPage.css');

var _GalleryPage2 = _interopRequireDefault(_GalleryPage);

var _GalleryItem = require('../GalleryItem');

var _GalleryItem2 = _interopRequireDefault(_GalleryItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

var title = 'Targets';

var GalleryPage = function (_Component) {
  (0, _inherits3.default)(GalleryPage, _Component);

  function GalleryPage(props) {
    (0, _classCallCheck3.default)(this, GalleryPage);

    var _this = (0, _possibleConstructorReturn3.default)(this, (GalleryPage.__proto__ || (0, _getPrototypeOf2.default)(GalleryPage)).call(this, props));

    _this.onResize = _this.onResize.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(GalleryPage, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.context.onSetTitle(title);
    }
  }, {
    key: 'onResize',
    value: function onResize() {
      console.log('resize');
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: _GalleryPage2.default.root, onResize: this.onResize },
        this.props.data.map(function (item, idx) {
          return _react2.default.createElement(_GalleryItem2.default, { key: idx, item: item });
        })
      );
    }
  }]);
  return GalleryPage;
}(_react.Component);

GalleryPage.propTypes = {
  data: _react.PropTypes.arrayOf(_react.PropTypes.object).isRequired
};
GalleryPage.contextTypes = {
  onSetTitle: _react.PropTypes.func.isRequired
};
exports.default = (0, _withStyles2.default)(GalleryPage, _GalleryPage2.default);

//# sourceMappingURL=GalleryPage-compiled.js.map