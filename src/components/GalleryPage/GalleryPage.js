/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './GalleryPage.css';
import GalleryItem from '../GalleryItem';

const title = 'Targets';

class GalleryPage extends Component {
  constructor(props) {
    super(props);
    this.onResize = this.onResize.bind(this);
  }

  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.context.onSetTitle(title);
  }

  onResize() {
    console.log('resize');
  }

  render() {
    return (
      <div className={s.root} onResize={this.onResize}>
        {this.props.data.map((item, idx) => {
          return (<GalleryItem key={idx} item={item} />);
        })}
      </div>
    );
  }

}

export default withStyles(GalleryPage, s);
