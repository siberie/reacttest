'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Navigation = require('./Navigation.css');

var _Navigation2 = _interopRequireDefault(_Navigation);

var _Link = require('../Link');

var _Link2 = _interopRequireDefault(_Link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Navigation = function (_Component) {
  (0, _inherits3.default)(Navigation, _Component);

  function Navigation() {
    (0, _classCallCheck3.default)(this, Navigation);
    return (0, _possibleConstructorReturn3.default)(this, (Navigation.__proto__ || (0, _getPrototypeOf2.default)(Navigation)).apply(this, arguments));
  }

  (0, _createClass3.default)(Navigation, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)(_Navigation2.default.root, this.props.className), role: 'navigation' },
        _react2.default.createElement(
          _Link2.default,
          { className: _Navigation2.default.link, to: '/about' },
          'About'
        ),
        _react2.default.createElement(
          _Link2.default,
          { className: _Navigation2.default.link, to: '/contact' },
          'Contact'
        ),
        _react2.default.createElement(
          'span',
          { className: _Navigation2.default.spacer },
          ' | '
        ),
        _react2.default.createElement(
          _Link2.default,
          { className: _Navigation2.default.link, to: '/login' },
          'Log in'
        ),
        _react2.default.createElement(
          'span',
          { className: _Navigation2.default.spacer },
          'or'
        ),
        _react2.default.createElement(
          _Link2.default,
          { className: (0, _classnames2.default)(_Navigation2.default.link, _Navigation2.default.highlight), to: '/register' },
          'Sign up'
        )
      );
    }
  }]);
  return Navigation;
}(_react.Component); /**
                      * React Starter Kit (https://www.reactstarterkit.com/)
                      *
                      * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                      *
                      * This source code is licensed under the MIT license found in the
                      * LICENSE.txt file in the root directory of this source tree.
                      */

Navigation.propTypes = {
  className: _react.PropTypes.string
};
exports.default = (0, _withStyles2.default)(Navigation, _Navigation2.default);

//# sourceMappingURL=Navigation-compiled.js.map