'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('isomorphic-style-loader/lib/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _TextBox = require('./TextBox.css');

var _TextBox2 = _interopRequireDefault(_TextBox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TextBox = function (_Component) {
  (0, _inherits3.default)(TextBox, _Component);

  function TextBox() {
    (0, _classCallCheck3.default)(this, TextBox);
    return (0, _possibleConstructorReturn3.default)(this, (TextBox.__proto__ || (0, _getPrototypeOf2.default)(TextBox)).apply(this, arguments));
  }

  (0, _createClass3.default)(TextBox, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: _TextBox2.default.root },
        this.props.maxLines > 1 ? _react2.default.createElement('textarea', (0, _extends3.default)({}, this.props, {
          className: _TextBox2.default.input,
          ref: 'input',
          key: 'input',
          rows: this.props.maxLines
        })) : _react2.default.createElement('input', (0, _extends3.default)({}, this.props, {
          className: _TextBox2.default.input,
          ref: 'input',
          key: 'input'
        }))
      );
    }
  }]);
  return TextBox;
}(_react.Component); /**
                      * React Starter Kit (https://www.reactstarterkit.com/)
                      *
                      * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                      *
                      * This source code is licensed under the MIT license found in the
                      * LICENSE.txt file in the root directory of this source tree.
                      */

TextBox.propTypes = {
  maxLines: _react.PropTypes.number
};
TextBox.defaultProps = {
  maxLines: 1
};
exports.default = TextBox;

//# sourceMappingURL=TextBox-compiled.js.map