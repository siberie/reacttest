'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _emptyFunction = require('fbjs/lib/emptyFunction');

var _emptyFunction2 = _interopRequireDefault(_emptyFunction);

var _App = require('./App.css');

var _App2 = _interopRequireDefault(_App);

var _Header = require('../Header');

var _Header2 = _interopRequireDefault(_Header);

var _Feedback = require('../Feedback');

var _Feedback2 = _interopRequireDefault(_Feedback);

var _Footer = require('../Footer');

var _Footer2 = _interopRequireDefault(_Footer);

var _SideMenu = require('../SideMenu');

var _SideMenu2 = _interopRequireDefault(_SideMenu);

var _MenuItem = require('../MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _reactDimensions = require('react-dimensions');

var _reactDimensions2 = _interopRequireDefault(_reactDimensions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var App = function (_Component) {
  (0, _inherits3.default)(App, _Component);

  function App() {
    (0, _classCallCheck3.default)(this, App);
    return (0, _possibleConstructorReturn3.default)(this, (App.__proto__ || (0, _getPrototypeOf2.default)(App)).apply(this, arguments));
  }

  (0, _createClass3.default)(App, [{
    key: 'getChildContext',
    value: function getChildContext() {
      var context = this.props.context;
      return {
        insertCss: context.insertCss || _emptyFunction2.default,
        onSetTitle: context.onSetTitle || _emptyFunction2.default,
        onSetMeta: context.onSetMeta || _emptyFunction2.default,
        onPageNotFound: context.onPageNotFound || _emptyFunction2.default
      };
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {

      console.log(_App2.default);
      console.log(_App2.default.root);
      var insertCss = this.props.context.insertCss;

      this.removeCss = insertCss(_App2.default);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.removeCss();
    }
  }, {
    key: 'render',
    value: function render() {
      console.log('container width' + this.props.containerWidth);

      return !this.props.error ? _react2.default.createElement(
        'div',
        { className: _App2.default.root },
        _react2.default.createElement(_Header2.default, null),
        _react2.default.createElement(
          'div',
          { className: _App2.default.container },
          _react2.default.createElement(
            _SideMenu2.default,
            { expanded: this.props.containerWidth >= 320, alignment: 'left' },
            _react2.default.createElement(_MenuItem2.default, { label: 'Home', image: { file: 'SideIcons.png', x: 0, y: -76, width: 38, height: 38 }, to: '/' }),
            _react2.default.createElement(_MenuItem2.default, { label: 'Messages', image: { file: 'SideIcons.png', x: 0, y: 114, width: 38, height: 38 }, to: '/messages' }),
            _react2.default.createElement(_MenuItem2.default, { label: 'Targets', image: { file: 'SideIcons.png', x: 0, y: 0, width: 38, height: 38 }, to: '/targets' })
          ),
          this.props.children
        )
      ) : this.props.children;
    }
  }]);
  return App;
}(_react.Component); /**
                      * React Starter Kit (https://www.reactstarterkit.com/)
                      *
                      * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                      *
                      * This source code is licensed under the MIT license found in the
                      * LICENSE.txt file in the root directory of this source tree.
                      */

App.propTypes = {
  context: _react.PropTypes.shape({
    insertCss: _react.PropTypes.func,
    onSetTitle: _react.PropTypes.func,
    onSetMeta: _react.PropTypes.func,
    onPageNotFound: _react.PropTypes.func
  }),
  children: _react.PropTypes.element.isRequired,
  error: _react.PropTypes.object
};
App.childContextTypes = {
  insertCss: _react.PropTypes.func.isRequired,
  onSetTitle: _react.PropTypes.func.isRequired,
  onSetMeta: _react.PropTypes.func.isRequired,
  onPageNotFound: _react.PropTypes.func.isRequired
};
exports.default = (0, _reactDimensions2.default)()(App);

//# sourceMappingURL=App-compiled.js.map