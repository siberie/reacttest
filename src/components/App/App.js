/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, {Component, PropTypes} from 'react';
import emptyFunction from 'fbjs/lib/emptyFunction';
import s from './App.css';
import Header from '../Header';
import Feedback from '../Feedback';
import Footer from '../Footer';
import SideMenu from '../SideMenu';
import MenuItem from '../MenuItem';
import Dimensions from 'react-dimensions'



class App extends Component {

  static propTypes = {
    context: PropTypes.shape({
      insertCss: PropTypes.func,
      onSetTitle: PropTypes.func,
      onSetMeta: PropTypes.func,
      onPageNotFound: PropTypes.func,
    }),
    children: PropTypes.element.isRequired,
    error: PropTypes.object,
  };

  static childContextTypes = {
    insertCss: PropTypes.func.isRequired,
    onSetTitle: PropTypes.func.isRequired,
    onSetMeta: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired,
  };

  getChildContext() {
    const context = this.props.context;
    return {
      insertCss: context.insertCss || emptyFunction,
      onSetTitle: context.onSetTitle || emptyFunction,
      onSetMeta: context.onSetMeta || emptyFunction,
      onPageNotFound: context.onPageNotFound || emptyFunction,
    };
  }

  componentWillMount() {

    console.log(s);
    console.log(s.root);
    const {insertCss} = this.props.context;
    this.removeCss = insertCss(s);
  }

  componentWillUnmount() {
    this.removeCss();
  }

  render() {
    console.log('container width' + this.props.containerWidth);

    return !this.props.error ? (

    <div className={s.root}>
        <Header />
        <div className={s.container}>
          <SideMenu expanded={this.props.containerWidth >= 320} alignment='left'>
            <MenuItem label="Home" image={{file: 'SideIcons.png', x: 0, y: -76, width:38, height:38}} to="/"/>
            <MenuItem label="Messages" image={{file: 'SideIcons.png', x: 0, y: 114, width:38, height:38}} to="/messages"/>
            <MenuItem label="Targets" image={{file: 'SideIcons.png', x: 0, y: 0, width:38, height:38}} to="/targets"/>
          </SideMenu>
          {this.props.children}
        </div>
        {/*<Feedback />*/}
        {/*<Footer />*/}
      </div>
    ) : this.props.children;
  }

}

export default Dimensions()(App);
