'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

require('babel-polyfill');

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _expressJwt = require('express-jwt');

var _expressJwt2 = _interopRequireDefault(_expressJwt);

var _expressGraphql = require('express-graphql');

var _expressGraphql2 = _interopRequireDefault(_expressGraphql);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _server = require('react-dom/server');

var _server2 = _interopRequireDefault(_server);

var _prettyError = require('pretty-error');

var _prettyError2 = _interopRequireDefault(_prettyError);

var _passport = require('./core/passport');

var _passport2 = _interopRequireDefault(_passport);

var _schema = require('./data/schema');

var _schema2 = _interopRequireDefault(_schema);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _assets = require('./assets');

var _assets2 = _interopRequireDefault(_assets);

var _config = require('./config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var server = global.server = (0, _express2.default)();

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
server.use(_express2.default.static(_path2.default.join(__dirname, 'public')));
server.use((0, _cookieParser2.default)());
server.use(_bodyParser2.default.urlencoded({ extended: true }));
server.use(_bodyParser2.default.json());

//
// Authentication
// -----------------------------------------------------------------------------
server.use((0, _expressJwt2.default)({
  secret: _config.auth.jwt.secret,
  credentialsRequired: false,
  /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
  getToken: function getToken(req) {
    return req.cookies.id_token;
  }
}));
server.use(_passport2.default.initialize());

server.get('/login/facebook', _passport2.default.authenticate('facebook', { scope: ['email', 'user_location'], session: false }));
server.get('/login/facebook/return', _passport2.default.authenticate('facebook', { failureRedirect: '/login', session: false }), function (req, res) {
  var expiresIn = 60 * 60 * 24 * 180; // 180 days
  var token = _jsonwebtoken2.default.sign(req.user, _config.auth.jwt.secret, { expiresIn: expiresIn });
  res.cookie('id_token', token, { maxAge: 1000 * expiresIn, httpOnly: true });
  res.redirect('/');
});

//
// Register API middleware
// -----------------------------------------------------------------------------
server.use('/graphql', (0, _expressGraphql2.default)(function (req) {
  return {
    schema: _schema2.default,
    graphiql: true,
    rootValue: { request: req },
    pretty: process.env.NODE_ENV !== 'production'
  };
}));

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
server.get('*', function () {
  var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res, next) {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            return _context2.delegateYield(_regenerator2.default.mark(function _callee() {
              var statusCode, template, data, css, context;
              return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      statusCode = 200;
                      template = require('./views/index.jade');
                      data = { title: '', description: '', css: '', body: '', entry: _assets2.default.main.js };


                      if (process.env.NODE_ENV === 'production') {
                        data.trackingId = _config.analytics.google.trackingId;
                      }

                      css = [];
                      context = {
                        insertCss: function insertCss(styles) {
                          console.log(typeof styles === 'undefined' ? 'undefined' : (0, _typeof3.default)(styles));
                          console.log(styles);

                          css.push(styles._getCss());
                        },
                        onSetTitle: function onSetTitle(value) {
                          return data.title = value;
                        },
                        onSetMeta: function onSetMeta(key, value) {
                          return data[key] = value;
                        },
                        onPageNotFound: function onPageNotFound() {
                          return statusCode = 404;
                        }
                      };
                      _context.next = 8;
                      return _routes2.default.dispatch({ path: req.path, query: req.query, context: context }, function (state, component) {
                        data.body = _server2.default.renderToString(component);
                        data.css = css.join('');
                      });

                    case 8:

                      res.status(statusCode);
                      res.send(template(data));

                    case 10:
                    case 'end':
                      return _context.stop();
                  }
                }
              }, _callee, undefined);
            })(), 't0', 2);

          case 2:
            _context2.next = 7;
            break;

          case 4:
            _context2.prev = 4;
            _context2.t1 = _context2['catch'](0);

            next(_context2.t1);

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined, [[0, 4]]);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}());

//
// Error handling
// -----------------------------------------------------------------------------
var pe = new _prettyError2.default();
pe.skipNodeFiles();
pe.skipPackage('express');

server.use(function (err, req, res, next) {
  // eslint-disable-line no-unused-vars
  console.log(pe.render(err)); // eslint-disable-line no-console
  var template = require('./views/error.jade');
  var statusCode = err.status || 500;
  res.status(statusCode);
  res.send(template({
    message: err.message,
    stack: process.env.NODE_ENV === 'production' ? '' : err.stack
  }));
});

//
// Launch the server
// -----------------------------------------------------------------------------
server.listen(_config.port, function () {
  /* eslint-disable no-console */
  console.log('The server is running at http://localhost:' + _config.port + '/');
});

//# sourceMappingURL=server-compiled.js.map