'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Router = require('react-routing/src/Router');

var _Router2 = _interopRequireDefault(_Router);

var _fetch = require('./core/fetch');

var _fetch2 = _interopRequireDefault(_fetch);

var _App = require('./components/App');

var _App2 = _interopRequireDefault(_App);

var _ContentPage = require('./components/ContentPage');

var _ContentPage2 = _interopRequireDefault(_ContentPage);

var _ContactPage = require('./components/ContactPage');

var _ContactPage2 = _interopRequireDefault(_ContactPage);

var _LoginPage = require('./components/LoginPage');

var _LoginPage2 = _interopRequireDefault(_LoginPage);

var _RegisterPage = require('./components/RegisterPage');

var _RegisterPage2 = _interopRequireDefault(_RegisterPage);

var _GalleryPage = require('./components/GalleryPage');

var _GalleryPage2 = _interopRequireDefault(_GalleryPage);

var _NotFoundPage = require('./components/NotFoundPage');

var _NotFoundPage2 = _interopRequireDefault(_NotFoundPage);

var _ErrorPage = require('./components/ErrorPage');

var _ErrorPage2 = _interopRequireDefault(_ErrorPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _Router2.default(function (on) {
  on('*', function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(state, next) {
      var component;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return next();

            case 2:
              component = _context.sent;
              return _context.abrupt('return', component && _react2.default.createElement(
                _App2.default,
                { context: state.context },
                component
              ));

            case 4:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());

  on('/contact', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt('return', _react2.default.createElement(_ContactPage2.default, null));

          case 1:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined);
  })));

  on('/login', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt('return', _react2.default.createElement(_LoginPage2.default, null));

          case 1:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined);
  })));

  on('/register', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4() {
    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            return _context4.abrupt('return', _react2.default.createElement(_RegisterPage2.default, null));

          case 1:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, undefined);
  })));

  on('/messages', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5() {
    return _regenerator2.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            return _context5.abrupt('return', _react2.default.createElement(_ContactPage2.default, null));

          case 1:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, undefined);
  })));

  on('/targets', function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(state) {
      var query, response, _ref7, targets;

      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              query = '/graphql?query={targets(name:"test"){name, image}}';

              console.log(query);
              _context6.next = 4;
              return (0, _fetch2.default)(query);

            case 4:
              response = _context6.sent;
              _context6.next = 7;
              return response.json();

            case 7:
              _ref7 = _context6.sent;
              targets = _ref7.data.targets;
              return _context6.abrupt('return', _react2.default.createElement(_GalleryPage2.default, { data: targets }));

            case 10:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, undefined);
    }));

    return function (_x3) {
      return _ref6.apply(this, arguments);
    };
  }());

  on('*', function () {
    var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(state) {
      var query, response, _ref9, data;

      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              query = '/graphql?query={content(path:"' + state.path + '"){path,title,content,component}}';
              _context7.next = 3;
              return (0, _fetch2.default)(query);

            case 3:
              response = _context7.sent;
              _context7.next = 6;
              return response.json();

            case 6:
              _ref9 = _context7.sent;
              data = _ref9.data;
              return _context7.abrupt('return', data && data.content && _react2.default.createElement(_ContentPage2.default, data.content));

            case 9:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, undefined);
    }));

    return function (_x4) {
      return _ref8.apply(this, arguments);
    };
  }());

  on('error', function (state, error) {
    return state.statusCode === 404 ? _react2.default.createElement(
      _App2.default,
      { context: state.context, error: error },
      _react2.default.createElement(_NotFoundPage2.default, null)
    ) : _react2.default.createElement(
      _App2.default,
      { context: state.context, error: error },
      _react2.default.createElement(_ErrorPage2.default, null)
    );
  });
}); /**
     * React Starter Kit (https://www.reactstarterkit.com/)
     *
     * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE.txt file in the root directory of this source tree.
     */

exports.default = router;

//# sourceMappingURL=routes-compiled.js.map