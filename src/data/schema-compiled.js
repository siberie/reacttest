'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphql = require('graphql');

var _me = require('./queries/me');

var _me2 = _interopRequireDefault(_me);

var _content = require('./queries/content');

var _content2 = _interopRequireDefault(_content);

var _target = require('./queries/target');

var _target2 = _interopRequireDefault(_target);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

var schema = new _graphql.GraphQLSchema({
  query: new _graphql.GraphQLObjectType({
    name: 'Query',
    fields: {
      me: _me2.default,
      content: _content2.default,
      targets: _target2.default
    }
  })
});

exports.default = schema;

//# sourceMappingURL=schema-compiled.js.map