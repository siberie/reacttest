'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var resolveExtension = function () {
  var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(path, extension) {
    var fileNameBase, ext, fileName;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            fileNameBase = (0, _path.join)(CONTENT_DIR, '' + (path === '/' ? '/index' : path));
            ext = extension;

            if (!ext.startsWith('.')) {
              ext = '.' + extension;
            }

            fileName = fileNameBase + ext;
            _context.next = 6;
            return fileExists(fileName);

          case 6:
            if (_context.sent) {
              _context.next = 9;
              break;
            }

            fileNameBase = (0, _path.join)(CONTENT_DIR, path + '/index');
            fileName = fileNameBase + ext;

          case 9:
            _context.next = 11;
            return fileExists(fileName);

          case 11:
            if (_context.sent) {
              _context.next = 13;
              break;
            }

            return _context.abrupt('return', { success: false });

          case 13:
            return _context.abrupt('return', { success: true, fileName: fileName });

          case 14:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function resolveExtension(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var resolveFileName = function () {
  var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(path) {
    var extensions, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, extension, maybeFileName;

    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            extensions = ['.jade', '.md', '.html'];
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context2.prev = 4;
            _iterator = (0, _getIterator3.default)(extensions);

          case 6:
            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
              _context2.next = 16;
              break;
            }

            extension = _step.value;
            _context2.next = 10;
            return resolveExtension(path, extension);

          case 10:
            maybeFileName = _context2.sent;

            if (!maybeFileName.success) {
              _context2.next = 13;
              break;
            }

            return _context2.abrupt('return', { success: true, fileName: maybeFileName.fileName, extension: extension });

          case 13:
            _iteratorNormalCompletion = true;
            _context2.next = 6;
            break;

          case 16:
            _context2.next = 22;
            break;

          case 18:
            _context2.prev = 18;
            _context2.t0 = _context2['catch'](4);
            _didIteratorError = true;
            _iteratorError = _context2.t0;

          case 22:
            _context2.prev = 22;
            _context2.prev = 23;

            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }

          case 25:
            _context2.prev = 25;

            if (!_didIteratorError) {
              _context2.next = 28;
              break;
            }

            throw _iteratorError;

          case 28:
            return _context2.finish(25);

          case 29:
            return _context2.finish(22);

          case 30:
            return _context2.abrupt('return', { success: false, fileName: null, extension: null });

          case 31:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this, [[4, 18, 22, 30], [23,, 25, 29]]);
  }));

  return function resolveFileName(_x3) {
    return _ref2.apply(this, arguments);
  };
}();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _jade = require('jade');

var _jade2 = _interopRequireDefault(_jade);

var _frontMatter = require('front-matter');

var _frontMatter2 = _interopRequireDefault(_frontMatter);

var _markdownIt = require('markdown-it');

var _markdownIt2 = _interopRequireDefault(_markdownIt);

var _graphql = require('graphql');

var _ContentType = require('../types/ContentType');

var _ContentType2 = _interopRequireDefault(_ContentType);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

var md = new _markdownIt2.default();

// A folder with Jade/Markdown/HTML content pages
var CONTENT_DIR = (0, _path.join)(__dirname, './content');

// Extract 'front matter' metadata and generate HTML
var parseContent = function parseContent(path, fileContent, extension) {
  var fmContent = (0, _frontMatter2.default)(fileContent);
  var htmlContent = void 0;
  switch (extension) {
    case '.jade':
      htmlContent = _jade2.default.render(fmContent.body);
      break;
    case '.md':
      htmlContent = md.render(fmContent.body);
      break;
    case '.html':
      htmlContent = fmContent.body;
      break;
    default:
      return null;
  }
  var smth = (0, _assign2.default)({ path: path, content: htmlContent }, fmContent.attributes);
  return smth;
};

var readFile = _bluebird2.default.promisify(_fs2.default.readFile);
var fileExists = function fileExists(filename) {
  return new _bluebird2.default(function (resolve) {
    _fs2.default.exists(filename, resolve);
  });
};

var content = {
  type: _ContentType2.default,
  args: {
    path: { type: new _graphql.GraphQLNonNull(_graphql.GraphQLString) }
  },
  resolve: function resolve(_ref3, _ref4) {
    var _this = this;

    var request = _ref3.request;
    var path = _ref4.path;
    return (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
      var _ref5, success, fileName, extension, source;

      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return resolveFileName(path);

            case 2:
              _ref5 = _context3.sent;
              success = _ref5.success;
              fileName = _ref5.fileName;
              extension = _ref5.extension;

              if (success) {
                _context3.next = 8;
                break;
              }

              return _context3.abrupt('return', null);

            case 8:
              _context3.next = 10;
              return readFile(fileName, { encoding: 'utf8' });

            case 10:
              source = _context3.sent;
              return _context3.abrupt('return', parseContent(path, source, extension));

            case 12:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this);
    }))();
  }
};

exports.default = content;

//# sourceMappingURL=content-compiled.js.map