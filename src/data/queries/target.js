import fs from 'fs';
import { join } from 'path';
import Promise from 'bluebird';
import jade from 'jade';
import fm from 'front-matter';
import MarkdownIt from 'markdown-it';


import TargetType from '../types/TargetType';


import {
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
} from 'graphql';

const md = new MarkdownIt();

// A folder with Jade/Markdown/HTML content pages
const CONTENT_DIR = join(__dirname, './content');

// Extract 'front matter' metadata and generate HTML
const parseContent = (path, fileContent, extension) => {
  var data = JSON.parse(fileContent);
  return data.targets;
};

const readFile = Promise.promisify(fs.readFile);
const fileExists = filename => new Promise(resolve => {
  fs.exists(filename, resolve);
});

async function resolveExtension(path, extension) {
  let fileNameBase = join(CONTENT_DIR, `${path === '/' ? '/index' : path}`);
  let ext = extension;
  if (!ext.startsWith('.')) {
    ext = `.${extension}`;
  }

  let fileName = fileNameBase + ext;

  if (!(await fileExists(fileName))) {
    fileNameBase = join(CONTENT_DIR, `${path}/index`);
    fileName = fileNameBase + ext;
  }

  if (!(await fileExists(fileName))) {
    return { success: false };
  }

  return { success: true, fileName };
}

async function resolveFileName(path) {
  const extensions = ['.json'];

  for (const extension of extensions) {
    const maybeFileName = await resolveExtension(path, extension);
    if (maybeFileName.success) {
      return { success: true, fileName: maybeFileName.fileName, extension };
    }
  }

  return { success: false, fileName: null, extension: null };
}

const target = {
  type: TargetType,
  args: {
    name: { type: new NonNull(StringType) },
  },
  async resolve({ request }, { name }) {
    const { success, fileName, extension } = await resolveFileName('targets');
    if (!success) {
      return null;
    }
    const source = await readFile(fileName, { encoding: 'utf8' });
    return parseContent(name, source, extension);
  },
};

export default target;
