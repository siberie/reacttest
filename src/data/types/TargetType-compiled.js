'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphql = require('graphql');

var TargetType = new _graphql.GraphQLList(new _graphql.GraphQLObjectType({
  name: 'Target',
  fields: {
    name: { type: new _graphql.GraphQLNonNull(_graphql.GraphQLString) },
    image: { type: new _graphql.GraphQLNonNull(_graphql.GraphQLString) }
  }
}));

// const TargetListType = new ListType

exports.default = TargetType;

//# sourceMappingURL=TargetType-compiled.js.map