import {
  GraphQLObjectType as ObjectType,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
  GraphQLList as ListType
} from 'graphql';

const TargetType = new ListType(new ObjectType({
  name: 'Target',
  fields: {
    name: { type: new NonNull(StringType) },
    image: { type:new NonNull(StringType) },
  },
}));

// const TargetListType = new ListType

export default TargetType;
